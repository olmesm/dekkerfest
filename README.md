# Dekkerfest

<http://dekkerfest.ml/>

## To start

```sh
# Setup Node
nvm use

# Install Dependencies
npm i

# To Dev
npm run ionic:serve

# To deploy PWA
git push
```

## Adding SVG's

Add addition SVG's to `./src/assets/logos`

Then run the script to add them to the export list.

```sh
node scripts/make-logo-file.js
```

## Additional Docs

- [Setting up SSL](./documents/ssl.md)

## Resources

- [Gitlab](https://gitlab.com)
- [Ionic 2](https://ionic.io)
