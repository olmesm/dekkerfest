# Setting up SSL

```sh
# Install Certbot
brew install certbot

# Generate Certificate
sudo certbot certonly -a manual -d dekkerfest.ml
```

Follow the prompts as seen on screen.

The file `.gitlab-ci.yml` copies the required verification files across to the correct location for after the app is built.

You will require this during the process of verifying you are the owner of the site.

Once completed copy the contents of the generated SSL certificate and key to the gitlab account.
