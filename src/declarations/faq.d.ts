declare interface Faq {
  question: String;
  answer: String;
  link?: String;
}
