declare interface TimeCardType {
  acts?: Act[];
  color: string,
  detail: string,
  time: string,
  title: string,
  linkText?: string,
  link?: string,
}

declare interface Act {
  act: string;
  stage: string;
}
