import { Component } from '@angular/core';

import { MorningPage } from '../morning/morning';
import { AfternoonPage } from '../afternoon/afternoon';
import { LocationPage } from '../location/location';
import { FaqPage } from '../faq/faq';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = MorningPage;
  tab2Root = AfternoonPage;
  tab3Root = LocationPage;
  tab4Root = FaqPage;

  constructor() {

  }
}
