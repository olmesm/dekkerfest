import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'page-afternoon',
  templateUrl: 'afternoon.html'
})
export class AfternoonPage implements OnInit{

  sessions: TimeCardType[];

  constructor(public navCtrl: NavController, public http: HttpClient) {

  }

  ngOnInit(){
    this.http.get('../../assets/data/afternoon-sessions.json')
      .subscribe((data: { sessions: TimeCardType[] }) => {
        this.sessions = data.sessions;
    })
  }

}
