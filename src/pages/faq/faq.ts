import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-faq',
  templateUrl: 'faq.html'
})
export class FaqPage implements OnInit {
  faqs: Faq[];

  constructor(public navCtrl: NavController, private http: HttpClient) {

  }

  ngOnInit() {
    this.http.get('../../assets/data/faqs.json')
      .subscribe((data: {faqs: Array<Faq>}) => {
        this.faqs = data.faqs;
      })
  }

}
