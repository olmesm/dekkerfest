import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'page-morning',
  templateUrl: 'morning.html'
})
export class MorningPage {

  sessions: TimeCardType[];

  constructor(public navCtrl: NavController, public http: HttpClient) {

  }

  ngOnInit(){
    this.http.get('../../assets/data/morning-sessions.json')
      .subscribe((data: {sessions: TimeCardType[]}) => {
        this.sessions = data.sessions;
    })
  }

}
