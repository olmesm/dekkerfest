import { Component, Input } from '@angular/core';

@Component({
  selector: 'faq-list',
  templateUrl: 'faq-list.html'
})
export class FaqListComponent {
  @Input() faqs: Faq[];

  constructor() { }

}
