import { Component, Input, OnInit } from '@angular/core';

export const colors = [
  'blue',
  'green',
  'red',
  'purple',
  'pink',
  'yellow',
];

const stageColorList = {
  anchor: 'blue',
  cove: 'pink',
  dekk: 'white',
  lighthouse: 'green',
  net: 'yellow',
  portside: 'red',
  starboard: 'purple',
};

@Component({
  selector: 'time-card',
  templateUrl: 'time-card.html'
})

export class TimeCardComponent implements OnInit{
  @Input() cardDetail: TimeCardType;
  cardClass: string;
  pillClass: string;
  textClass: string;

  constructor() { }

  ngOnInit() {
    this.createRandomColor();
  }

  createRandomColor() {
    const randomClass = colors[Math.floor(Math.random() * colors.length)];

    this.cardClass = `border--bottom border--bottom__${randomClass}`;
    this.pillClass = `and-${randomClass}`;
    this.textClass = `text--${randomClass}`
  }

  getColor(stage: string): string {
    return `and-${stageColorList[stage]}`;
  }
}
