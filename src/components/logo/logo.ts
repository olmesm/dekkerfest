import { Component, Input } from '@angular/core';

import * as svgList from '../../assets/logos/svg-list'

@Component({
  selector: 'logo',
  templateUrl: 'logo.html'
})

export class LogoComponent {
  @Input() type: string;

  constructor() { }

  get svgImage() {
    return svgList[`${this.type}Svg`];
  }
}
