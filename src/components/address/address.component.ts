import {Component, Input} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-address',
  templateUrl: './address.html',
})
export class AddressComponent {

  @Input() line1: string;
  @Input() line2: string;
  @Input() line3: string;
  @Input() postcode: string;

  constructor(public sanitizer: DomSanitizer){
  }
}
