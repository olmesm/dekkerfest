import { IonicModule } from 'ionic-angular';
import { NgModule } from '@angular/core';

import { PipesModule } from '../pipes/pipes.module';
import { AddressComponent } from './address/address.component';
import { TimeCardComponent } from './timeCard/timeCard.component';

import { FaqAccordionComponent } from './faqAccordion/faqAccordion';
import { FaqListComponent } from './faqList/faqList';
import { LogoComponent } from './logo/logo';
import { TimeListingComponent } from './timeListing/timeListing.component';
import { MapComponent } from './map/map.component';

@NgModule({
	declarations: [
    AddressComponent,
    FaqAccordionComponent,
    FaqListComponent,
    LogoComponent,
    MapComponent,
    TimeCardComponent,
    TimeListingComponent,
  ],
  imports: [
    IonicModule,
    PipesModule,
  ],
	exports: [
    AddressComponent,
    FaqAccordionComponent,
    FaqListComponent,
    LogoComponent,
    MapComponent,
    TimeCardComponent,
    TimeListingComponent,
  ]
})
export class ComponentsModule {}
