import { Component, Input } from '@angular/core';

@Component({
  selector: 'time-listing',
  templateUrl: 'time-listing.html'
})

export class TimeListingComponent {
  @Input() listings: TimeCardType[];

  constructor() { }
}
