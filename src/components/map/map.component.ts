import {Component, Input} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-map',
  templateUrl: './map.html',
})
export class MapComponent{

  @Input() mapSrc: string;

  constructor(public sanitizer: DomSanitizer){
  }
}
