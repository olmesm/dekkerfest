import { Component, Input, Output, ViewChild, ElementRef, Renderer } from '@angular/core';

@Component({
  selector: 'faq-accordion',
  templateUrl: 'faq-accordion.html'
})
export class FaqAccordionComponent {

  @Input() faq: Faq;
  @Output() expanded = false;
  @ViewChild('expandWrapper', {read: ElementRef}) expandWrapper;

  constructor(public renderer: Renderer) { }

  expandFaq(){
    if(this.expanded){
      this.renderer.setElementStyle(this.expandWrapper.nativeElement, 'height', '0');
      this.renderer.setElementStyle(this.expandWrapper.nativeElement, 'max-height', '0px');
    } else {
      this.renderer.setElementStyle(this.expandWrapper.nativeElement, 'height', '100%');
      this.renderer.setElementStyle(this.expandWrapper.nativeElement, 'max-height', '500px');
    }
    this.expanded = !this.expanded;
  }
}
