import { IonicModule } from 'ionic-angular';
import { NgModule } from '@angular/core';

import { SafePipe } from './safe/safe';

@NgModule({
	declarations: [SafePipe],
	imports: [IonicModule],
	exports: [SafePipe]
})

export class PipesModule {}
