import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HttpClientModule } from '@angular/common/http';

import { TabsPage } from '../pages/tabs/tabs';
import { MorningPage} from '../pages/morning/morning';
import { AfternoonPage} from '../pages/afternoon/afternoon';
import { LocationPage} from '../pages/location/location';
import { FaqPage} from '../pages/faq/faq';

import { ComponentsModule } from './../components/components.module';
import { PipesModule } from './../pipes/pipes.module';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@NgModule({
  declarations: [
    MyApp,
    MorningPage,
    AfternoonPage,
    LocationPage,
    FaqPage,
    TabsPage,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, { mode: 'md' }),
    ComponentsModule,
    HttpClientModule,
    PipesModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    MorningPage,
    AfternoonPage,
    FaqPage,
    LocationPage,
    TabsPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
  ],
  exports: [
    ComponentsModule,
    PipesModule,
  ]
})
export class AppModule {}
