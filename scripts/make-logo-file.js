const { readFileSync, writeFileSync } = require('fs');
const glob = require('glob').sync;
const rimraf = require('rimraf').sync;

const caseCase = require('../scripts/camel-case');

const dir = './src/assets/logos';
const svgExportFile = `${dir}/svg-list.js`;

// delete any existing export file
rimraf(svgExportFile);

const svgList = glob(`${dir}/**/*.svg`);

// Assume could return a mix of SVG files and nulls (it won't)
const svgNamesUnfiltered = svgList.map(n => {
  const match = n.match(/([A-z|-]*)(.svg)/);

  // Will return an array of the regex match
  //   item 1 will be an exact match
  //   unless no match, then only null is returned (not an array)
  return match && caseCase(match[1]);
});

// fiter out any null values
const svgNames = svgNamesUnfiltered.filter(n => !!n);

const exportedSvgList = svgList.map((f, i) =>
`export const ${svgNames[i]}Svg = \`
  ${readFileSync(f, 'utf8').trim()}
\`;
`
);

writeFileSync(svgExportFile, exportedSvgList.join('\n'));
