module.exports = s => s.toLowerCase().replace(/-([a-z])/gi, (s, firstLetter) => firstLetter.toUpperCase());
